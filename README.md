# LinuxFS
***LinuxFS*** is a copy of the [LFScript](https://lfscript.org) code and modded for use. I don't see anything ever really working well. I also have no idea where this gets worked on. Was Wuala for a while (that was good back then), there's a spot at [Ubuntu](https://launchpad.net/lfscript) - doesn't seem to get used much.

So I decided to pull the latest (a 3 year old stale version, at the starting point). I will use [Debian](https://debian.org) as a host for building.

## Features
- The same as the original set of scripts


## Planned Features
- Choice of building
 - Latest Stable LFS 
 - Latest Stable BLFS
 - Build SVN versions


## Documentation
As the project grows so will the Docs. We will use the [WIKI](../../wikis/home) as well as the (upcoming) [project's Linux website](https://cybernux.org).

## License
As the original, MIT.
See [LICENSE](LICENSE) for details.